import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Steganography {
    public static final String FIXED_CONTAINER = "src\\main\\resources\\fixedContainer.txt";
    public static final String HIDDEN = "src\\main\\resources\\hidden.txt";
    public static final String EXTRACTED = "src\\main\\resources\\extracted.txt";

    public static void containerFix(String containerPath) throws IOException {
        File container = new File(containerPath);
        File fixedContainer = new File(FIXED_CONTAINER);
        if (!fixedContainer.exists()) {
            fixedContainer.createNewFile();
        }

        String line;
        FileWriter fileWriter = new FileWriter(fixedContainer);

        BufferedReader bufferedReader = new BufferedReader(new FileReader(container));
        String res = "";
        while ((line = bufferedReader.readLine()) != null) {
            int pos = line.indexOf("  ");
            while (pos != -1) {
                int i = 0;
                while (line.charAt(pos + i) == ' ') {
                    i++;
                }
                line = line.substring(0, pos) + line.substring(pos + i - 1);
                pos = line.indexOf("  ");
            }
            res += line + '\n';
        }

        res = res.substring(0, Math.max(res.length() - 1, 0));
        fileWriter.write(res);
        fileWriter.close();
    }

    public static boolean byteCheck(BufferedReader container, FileInputStream file) throws IOException {
        int spaceNum = 0;
        String line;
        while ((line = container.readLine()) != null) {
            int pos = 0;
            while (pos != -1) {
                pos = line.indexOf(' ', pos + 1);
                spaceNum++;
            }
        }

        if (spaceNum < file.available() * 8) {
            return false;
        } else {
            return true;
        }
    }

    public static void hide(String filePath) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(filePath);
        BufferedReader bufferedReader = new BufferedReader(new FileReader(FIXED_CONTAINER));

        File hidden = new File(HIDDEN);

        if (!hidden.exists()) {
            hidden.createNewFile();
        }
        FileWriter fileWriter = new FileWriter(hidden);
        String res = "";

        if (!byteCheck(new BufferedReader(new FileReader(FIXED_CONTAINER)), new FileInputStream(filePath))) {
            System.out.println("Слишком маленький контейнер");
            return;
        }

        int c = fileInputStream.read();

        String containerLine;

        while ((containerLine = bufferedReader.readLine()) != null) {
            res += containerLine + '\n';
        }

        res = res.substring(0, Math.max(res.length() - 1, 0));
        int pos = res.indexOf(' ');

        while (c != -1) {
            String binaryLine = Integer.toBinaryString(c);
            while (binaryLine.length() != 8) {
                binaryLine = '0' + binaryLine;
            }
            for (int i = 0; i < binaryLine.length(); i++) {
                if (binaryLine.charAt(i) == '1') {
                    res = res.substring(0, pos) + ' ' + res.substring(pos);
                    pos++;
                }
                pos = res.indexOf(' ', pos + 1);
            }
            c = fileInputStream.read();
        }
        fileWriter.write(res);
        fileWriter.close();
    }

    public static void extract(String containerPath) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(containerPath));
        File resultFile = new File(EXTRACTED);
        String extractedString = "";

        if (!resultFile.exists()) {
            resultFile.createNewFile();
        }

        FileWriter fileWriter = new FileWriter(resultFile);

        String line;
        String containerLine = "";
        while ((line = bufferedReader.readLine()) != null) {
            containerLine += line;
        }

        int pos = containerLine.indexOf(' ');

        String binaryLine = "";
        List<Byte> bytes = new ArrayList<>();

        while (pos != -1) {
            if (containerLine.charAt(pos + 1) == ' ') {
                binaryLine += '1';
                pos++;
            } else {
                binaryLine += '0';
            }

            if (binaryLine.length() == 8) {
                int _byte = Integer.parseInt(binaryLine, 2);
                if (_byte != 0) {
                    bytes.add((byte) _byte);
                }
                binaryLine = "";
            }
            pos = containerLine.indexOf(' ', pos + 1);
        }

        byte[] _bytes = new byte[bytes.size()];
        for (int i = 0; i < bytes.size(); i++) {
            _bytes[i] = bytes.get(i);
        }

        String s = new String(_bytes, StandardCharsets.UTF_8);
        fileWriter.write(s);
        fileWriter.close();
    }


    public static void main(String[] args) throws IOException {


        Scanner in = new Scanner(System.in);
        System.out.println("1-hide\n2-extract");
        String type = in.next();

        switch (type) {
            case "1":
                containerFix("src\\main\\resources\\container.txt");
                hide("src\\main\\resources\\toHide.txt");
                break;
            case "2":
                extract("src\\main\\resources\\hidden.txt");
                break;
            default:
                System.out.println("Unknown symbol");
        }

    }
}
